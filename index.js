const puppeteer = require('puppeteer');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const URL = 'https://www.imdb.com/list/ls055592025/';

function writeToFile(outputFileName, data) {
    fs.writeFileSync(outputFileName, JSON.stringify(data));
}

function wait(ms) {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
}


(async () => {

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(URL, {
        waitUntil: 'networkidle0'
    });

    // Get the height of the rendered page
    const bodyHandle = await page.$('body');
    const { height } = await bodyHandle.boundingBox();
    await bodyHandle.dispose();

    // Scroll one viewport at a time, pausing to let content load
    const viewportHeight = page.viewport().height;
    let viewportIncr = 0;
    while (viewportIncr + viewportHeight < height) {
        await page.evaluate(_viewportHeight => {
            window.scrollBy(0, _viewportHeight);
        }, viewportHeight);
        await wait(20);
        viewportIncr = viewportIncr + viewportHeight;
    }

    // Scroll back to top
    await page.evaluate(_ => {
        window.scrollTo(0, 0);
    });

    // Some extra delay to let images load
    await wait(100);


    const data = await page.evaluate(async () => {

        const images = Array.from(
            document.querySelectorAll('.lister-item.mode-detail .loadlate')
        ).map(node => node.src);

        const titles = Array.from(
            document.querySelectorAll('.lister-item.mode-detail .lister-item-header > a')
        ).map(node => node.innerText);

        const genres = Array.from(
            document.querySelectorAll('.lister-item.mode-detail p.text-muted.text-small span.genre')
        ).map(node => node.innerText);

        const ratings = Array.from(
            document.querySelectorAll('.lister-item.mode-detail .ipl-rating-star.small > span.ipl-rating-star__rating')
        ).map(node => node.innerText);

        const descriptions = Array.from(
            document.querySelectorAll('#main > div > div.lister.list.detail.sub-list > div.lister-list > div > div.lister-item-content > p:nth-child(5)')
        ).map(n => n.innerText);

        const directorAndCast = Array.from(
            document.querySelectorAll('#main > div > div.lister.list.detail.sub-list > div.lister-list > div > div.lister-item-content > p:nth-child(6)')
        ).map(n => {
            const [ director = '', cast = '' ] = n.innerText.split('|')
            return {
                director: director.replace('Director: ', '').trim(),
                cast: cast.replace('Stars: ', '').trim()
            }
        });
        

        return {
            images,
            titles,
            genres,
            ratings,
            descriptions,
            directorAndCast
        };

    });

    const output = buildOutput(data);
    writeToFile('db.json', output);

    await browser.close();

})();

function buildOutput(data) {
    const { images, titles, genres, ratings, descriptions, directorAndCast } = data;
    const numberOfMovies = titles.length;
    const movies = [];
    const genreSet = new Set([]);

    for (let i = 0; i < numberOfMovies; i++) {

        movies.push(new Movie(
            titles[i],
            descriptions[i],
            images[i],
            ratings[i],
            genres[i].split(',').map(g => g.trim()),
            directorAndCast[i].director,
            directorAndCast[i].cast.split(',').map(s => s.trim())
        ));

        const genreArray = genres[i].split(',');
        for (let j = 0; j < genreArray.length; j++) {
            genreSet.add(genreArray[j].trim());
        }
    };

    const genresWithIds = [...genreSet].map(g => new Genre(g));

    return {
        movies,
        genres: genresWithIds,
        users: []
    };
}


function Movie(title, description, cover, rating, genre, director = '', cast = []) {
    this.id = uuidv4();
    this.title = title;
    this.description = description;
    this.cover = cover;
    this.rating = rating;
    this.genres = genre;
    this.director = director;
    this.cast = cast;
}

function Genre(title) {
    this.id = uuidv4();
    this.title = title;
}
